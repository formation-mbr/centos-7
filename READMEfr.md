# Script de configuration post-installation pour les serveurs Centos 7

(c) Niki Kovacs, 2020

Modification : MBaroyer, traduction en fr, 2020

Ce referentiel fournit un script "automagique" de configuraiton 
post-installation pour des serveurs fonctionnant sous CentOs7 ainsi qu'une 
collection de scripts d'aide et des modeles de fichiers de configuration pour 
les services communs.

## En resume

Effectuez les etapes suivantes.

  1. Installez un systeme CentOs 7 minimum.

  2. Crez un utilisateur non racine avec des privileges d'administrateur

  3. Installer Git : sudo yum install git

  4. Saississez le script : `git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Placez vous dans le nouveau repertoire: `cd centos-7`

  6. Executez le script : `sudo ./centos-setup.sh --setup`

  7. Prenez une tasse de cafe pendant que le scenario fait tout le travail.

  8. Redemarrez.


## Personnalisation d'un serveur CentOs

Transformer une installation CentOs minimale en un serveur fonctionnel bout 
toujours jusqu'a une serie d'operations plus ou moins longues. Votre parcours 
peut varie bien sur, mais voici ce que je fais habituellement sur une nouvelle 
installation CentOs:

  * Personnaliser le shell Bash : prompt, aliases, etc.

  * Personnaliser l'editeur Vim.

  * Configurer les depots officiels et tiers.

  * Installer un ensemble complet d'outils en ligne de commande.

  * Supprimer une poignee de paquets inutiles.

  * Autoriser l'utilisateur administrateur a acceder aux journaux du systeme.

  * Desactiver l'IPv6 et reconfigurer certains services en consequence.
  
  * Configurer un mot de passe persistant pour `sudo`.

  * Etc.

Le script centos-setup.sh effectue toutes ces operations.

Configurer Bash et Vim et definir une resolution de console par defaut plus 
lisible:

```
# ./centos-setup.sh --shell
```

Mettre en place des depots officiels et des depots de tiers :

```
# ./centos-setup.sh --repos
```

Installez les groupes de paquets Core et Base ainsi que quelques outils 
supplementaires:

```
# ./centos-setup.sh --extra
```

Retirez une poignee de paquets inutiles:

```
# ./centos-setup.sh --prune
```

Autoriser l'utilisateur administrateur a acceder aux journaux du systeme:

```
# ./centos-setup.sh --logs
```

Desactivez IPv6 et reconfigurez les services de base en consequence:

```
# ./centos-setup.sh --ipv4
```

Configurer un mot de passe persistent pour sudo:

```
# ./centos-setup.sh --sudo
```

Realisez tout cela en une seule fois:

```
# ./centos-setup.sh --setup
```

Deshabillez les paquets et revenez a un systeme de base ameliore:

```
# ./centos-setup.sh --strip
```

Afficher les messages d'aides:

```
# ./centos-setup.sh --help
```

Si vous voulez savoir ce qui se passe exactement sous le capot, ouvrez un 
deuxieme terminal:

```
$ tail -f /tmp/centos-setup.log
```

